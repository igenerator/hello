<?php

use Hello\Facades\Hello;

if(!function_exists('greet'))
{
    function greet(): string
    {
        return Hello::greet();
    }
}
