<?php

namespace Hello\Providers;

use Illuminate\Support\ServiceProvider;
use Hello\Classes\Hello;
use Hello\Commands\GreetMe;

class HelloServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerBindings();
        $this->registerCommands();
        $this->registerPublishables();
    }

    public function boot()
    {
        $this->registerConfigs();
    }

    public function registerBindings()
    {
        $this->app->singleton('hello', Hello::class);
    }

    public function registerCommands()
    {
        if($this->app->runningInConsole())
        {
            $this->commands([
                GreetMe::class,
            ]);
        }
    }

    public function registerConfigs()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/greetings.php', 'greetings');
    }


    public function registerPublishables()
    {
        $this->publishes([
            __DIR__.'/../../config/greetings.php' => config_path('greetings.php')
        ], 'hello-config');
    }
}
