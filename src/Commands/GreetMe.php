<?php

namespace Hello\Commands;

use Illuminate\Console\Command;

class GreetMe extends Command
{
    protected $signature = 'greet:me';

    protected $description = 'Greets you';

    public function handle(): int
    {
        $this->alert(greet());
        return self::SUCCESS;
    }
}
