<?php

namespace Hello\Classes;

class Hello
{
    private function pick($greetings = false) : string
    {
        if($greetings === false)
        {
            $greetings = config('greetings', []);
        }
        if(is_array($greetings))
        {
            $key = array_rand($greetings);
            return $this->pick($greetings[$key]);
        }
        elseif(mb_strlen($greetings) > 0)
        {
            return $greetings;
        }
        else
        {
            return 'Hello';
        }
    }

    public function greet()
    {
        return $this->pick();
    }
}
