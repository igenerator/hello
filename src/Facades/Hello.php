<?php

namespace Hello\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string greet()
 */
class Hello extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'hello';
    }
}
